# Differential sync

This is a basic RTC app, built with NestJS and React JS frameworks.

Concurrency is handled by [**differential synchronization**](https://neil.fraser.name/writing/sync/)
algorithm, created by Neil Fraser.

More on that: [Differential sync Google presentation](https://youtu.be/S2Hp_1jqpY8?si=W7REKp6iOxtEYIBO)


## How to run locally:

### Pre-requirements:

1. node js, minimum v18.20.2, installed globally:
https://nodejs.org/en/download/package-manager

2. NPM, minimum v10.5.0, installed globally (should be included in the above node installation)

3. MySQL server running


### Environment variables:

differential-sync-backend/.env
```
DB_HOST=localhost
DB_PORT=3306
DB_NAME=differential_sync
DB_USERNAME=root
DB_PASSWORD=root1234

```

### Installation:

There are two projects packed in the root folder FE and BE ones. Both should be running.

To run:
#### 1. differential-sync-backend:

```
cd differential-sync-backend

npm install

npm run start:dev
```

To run:
#### 2. differential-sync-frontend:


```
cd differential-sync-frontend

npm install

npm run dev
```