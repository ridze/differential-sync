export type File = {
  id: string;
  name: string;
  text: string;
  version: number;
};

export type FileSyncInitDto = {
  clientId: string;
  fileId: string;
  fileVersion: number;
}
