import { Injectable } from '@nestjs/common';
import { CreateFileDto } from './dto/create-file.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from './entities/file.entity';
import { Repository } from 'typeorm';
import { FileVersionSnapshot } from './entities/fileVersionSnapshot.entity';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(File)
    private filesRepository: Repository<File>,
    @InjectRepository(FileVersionSnapshot)
    private fileVersionSnapshotRepository: Repository<FileVersionSnapshot>,
  ) {}

  async create(createFileDto: CreateFileDto) {
    try {
      const fileObj = this.filesRepository.create({
        name: 'A new file!',
        text: 'A sample text.',
        ...createFileDto,
      });

      await this.filesRepository.save(fileObj);

      const fileVersionObj = this.fileVersionSnapshotRepository.create({
        text: fileObj.text,
        version: fileObj.version,
        fileId: fileObj.id,
      });

      await this.fileVersionSnapshotRepository.save(fileVersionObj);

      return fileObj;
    } catch (error) {
      throw error;
    }
  }

  findAll() {
    return this.filesRepository.find();
  }

  findOne(id: string) {
    return this.filesRepository.findOneBy({ id });
  }

  // Could still be used to update a files name, for example
  // update(id: number, updateFileDto: UpdateFileDto) {
  //   return `This action updates a #${id} file`;
  // }

  remove(id: string) {
    return this.filesRepository.delete(id);
  }

  async getFileVersionSnapshot(fileId: string, version: number) {
    return this.fileVersionSnapshotRepository.findOneBy({
      fileId,
      version,
    });
  }
}
