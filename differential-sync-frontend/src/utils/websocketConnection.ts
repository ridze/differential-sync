import {io} from 'socket.io-client';
import {Edit} from "../components/FileEditor.tsx";

export enum ServerMessageType {
  SERVER_TEXT_CHANGE = 'SERVER_TEXT_CHANGE',
  SERVER_ACK_SIGNAL = 'SERVER_ACK_SIGNAL',
}

export enum ClientMessageType {
  CLIENT_TEXT_CHANGE = 'CLIENT_TEXT_CHANGE',
  CLIENT_ACK_SIGNAL = 'CLIENT_ACK_SIGNAL',
}

type ServerTextChangePayload = { edits: Edit[] };
type ServerAckSignalPayload = { nCount: number };

type ClientTextChangePayload = { edits: Edit[] };
type ClientAckSignalPayload = { mCount: number };

export type ServerMessage = {
  fileId: string;
  type: ServerMessageType.SERVER_TEXT_CHANGE;
  payload: ServerTextChangePayload;
} | {
  fileId: string;
  type: ServerMessageType.SERVER_ACK_SIGNAL;
  payload: ServerAckSignalPayload;
};

export type ClientMessage = {
  fileId: string;
  type: ClientMessageType.CLIENT_TEXT_CHANGE;
  payload: ClientTextChangePayload;
} | {
  fileId: string;
  type: ClientMessageType.CLIENT_ACK_SIGNAL;
  payload: ClientAckSignalPayload;
};

export type MessageHandler = (message: ServerMessage) => void;

const ioInstance = io('http://localhost:4000/file-sync', {
  autoConnect: false
});

ioInstance.on('disconnect', (reason, description) => {
  // console.log('CLIENT DISCONNECTED', reason, description);
});

ioInstance.on('connect', () => {
  console.log('CLIENT CONNECTED', ioInstance.id);
});

ioInstance.on('message', (incomingMessage: ServerMessage) => {
  // console.log('MESSAGE ARRIVED', incomingMessage);
});

// TODO consider file
export class WebSocketConnection {
  private static ws = ioInstance;

  public static get clientId() {
    return this.ws.id;
  }

  public static connect(onMessage: MessageHandler) {
    this.ws.connect();
    this.ws.on('message', onMessage);
  }

  public static ensureConnection(onMessage: MessageHandler) {
    if (this.ws.connected) {
      console.log('ALREADY CONNECTED TO WS', this.ws);
      return;
    }

    // console.log('CONNECTING');
    return this.connect(onMessage);
  }

  public static disconnect() {
    // console.log('DISCONNECTING');
    this.ws.disconnect();
  }

  public static sendMessage(message: ClientMessage) {
    this.ws.send(message);
  }
}