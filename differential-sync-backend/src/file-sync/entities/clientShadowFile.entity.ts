import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { File } from '../../file/entities/file.entity';
import { Client } from './client.entity';
import { FileVersionSnapshot } from '../../file/entities/fileVersionSnapshot.entity';

@Entity('clientShadowFiles')
export class ClientShadowFile {
  @Column('longtext')
  text: string;

  @Column('int')
  nCount: number;

  @Column('int')
  mCount: number;

  // Version the shadow was made of
  @ManyToOne(
    () => FileVersionSnapshot,
    (fileVersionSnapshot) => fileVersionSnapshot.shadowFiles,
  )
  initialFileVersionSnapshot: FileVersionSnapshot;

  @Column('uuid')
  initialFileVersionSnapshotId: string;

  @ManyToOne(() => File, (file) => file.shadowFiles)
  @JoinColumn()
  file: File;

  @PrimaryColumn('uuid')
  fileId: string;

  @ManyToOne(() => Client, (client) => client.shadowFiles, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn()
  client: Client;

  @PrimaryColumn('uuid')
  clientId: string;
}
