import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { File } from './file.entity';
import { ClientShadowFile } from '../../file-sync/entities/clientShadowFile.entity';

@Entity('fileVersionSnapshots')
export class FileVersionSnapshot {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('longtext')
  text: string;

  @Column('int', { default: 1 })
  version: number;

  @ManyToOne(() => File, (file) => file.versionSnapshots)
  file: File;

  @Column('uuid')
  fileId: string;

  @OneToMany(
    () => ClientShadowFile,
    (clientShadowFile) => clientShadowFile.initialFileVersionSnapshot,
  )
  shadowFiles: ClientShadowFile[];
}
