import { Edit } from './update-file.dto';

export enum ServerMessageType {
  SERVER_TEXT_CHANGE = 'SERVER_TEXT_CHANGE',
  SERVER_ACK_SIGNAL = 'SERVER_ACK_SIGNAL',
}

export enum ClientMessageType {
  CLIENT_TEXT_CHANGE = 'CLIENT_TEXT_CHANGE',
  CLIENT_ACK_SIGNAL = 'CLIENT_ACK_SIGNAL',
}

type ServerTextChangePayload = { edits: Edit[] };
type ServerAckSignalPayload = { nCount: number };

type ClientTextChangePayload = { edits: Edit[] };
type ClientAckSignalPayload = { mCount: number };

export type ServerMessage =
  | {
      fileId: string;
      type: ServerMessageType.SERVER_TEXT_CHANGE;
      payload: ServerTextChangePayload;
    }
  | {
      fileId: string;
      type: ServerMessageType.SERVER_ACK_SIGNAL;
      payload: ServerAckSignalPayload;
    };

export type ClientMessage =
  | {
      fileId: string;
      type: ClientMessageType.CLIENT_TEXT_CHANGE;
      payload: ClientTextChangePayload;
    }
  | {
      fileId: string;
      type: ClientMessageType.CLIENT_ACK_SIGNAL;
      payload: ClientAckSignalPayload;
    };
