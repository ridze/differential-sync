import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { FileVersionSnapshot } from './fileVersionSnapshot.entity';
import { ClientShadowFile } from '../../file-sync/entities/clientShadowFile.entity';
import { ClientBackupFile } from '../../file-sync/entities/clientBackupFile.entity';

@Entity('files')
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  name: string;

  @Column('longtext')
  text: string;

  @Column('int', { default: 1 })
  version: number;

  @OneToMany(
    () => FileVersionSnapshot,
    (fileVersionSnapshot) => fileVersionSnapshot.file,
  )
  versionSnapshots: FileVersionSnapshot[];

  @OneToMany(
    () => ClientShadowFile,
    (clientShadowFile) => clientShadowFile.file,
  )
  shadowFiles: ClientShadowFile[];

  @OneToMany(
    () => ClientBackupFile,
    (clientBackupFile) => clientBackupFile.file,
  )
  backupFiles: ClientBackupFile[];
}
