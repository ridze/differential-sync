import DMP, { Patch } from './diffMatchPatch';

export type MainFile = {
  id: string;
  name: string;
  text: string;
  version: number;
};

export default class MainText {
  static filesCached = false;
  private static readonly mainTexts: Record<string, MainFile> = {};

  static getMainFile(fileId: string): MainFile | undefined {
    return this.mainTexts[fileId];
  }

  static setMainFile(fileId: string, newFile: MainFile): void {
    this.mainTexts[fileId] = newFile;
    this.filesCached = true;
  }

  static applyPatches(fileId: string, patch: Patch[]) {
    const mainFile = this.getMainFile(fileId);

    if (mainFile) {
      const [patchedText] = DMP.patch_apply(patch, mainFile.text);
      this.mainTexts[fileId].version += 1;
      this.mainTexts[fileId].text = patchedText;
    }
  }

  static getFilesArray() {
    return Object.values(this.mainTexts);
  }

  // static applyEdit(fileId: string, edit: Edit) {
  //   const mainText = this.getMainFile(fileId).text;
  //
  //   if (mainText) {
  //     const patches = DMP.patch_make(mainText, edit.diffs);
  //     return DMP.patch_apply(patches, mainText);
  //   }
  // }
  //
  // static applyEdits(fileId: string, edits: Edit[]) {
  //   edits.forEach((edit) => {
  //     const result = this.applyEdit(fileId, edit);
  //     console.log('EDIT', edit, 'RESULT', result);
  //   });
  // }
}
