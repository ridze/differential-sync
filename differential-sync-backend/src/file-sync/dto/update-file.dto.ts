export type FileDiff = [number, string];
export type FileDiffs = FileDiff[];

export type Edit = {
  diffs: FileDiffs;
  mCount: number;
  nCount: number;
};

export type Shadow = {
  text: string;
  mCount: number;
  nCount: number;
};

export type Backup = {
  text: string;
  mCount: number;
};
