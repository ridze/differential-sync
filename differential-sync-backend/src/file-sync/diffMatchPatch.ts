import { diff_match_patch, patch_obj } from 'diff-match-patch';

export type Patch = { new (): patch_obj };

const DMP = new diff_match_patch();
export default DMP;
