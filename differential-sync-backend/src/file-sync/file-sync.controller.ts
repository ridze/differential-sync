import { Controller, Post, Body } from '@nestjs/common';
import { FileSyncService } from './file-sync.service';
import { FileSyncInitDto } from './dto/file-sync-init.dto';

@Controller('file-sync-init')
export class FileSyncController {
  constructor(private readonly fileSyncService: FileSyncService) {}

  @Post()
  async initiateFileSynchronization(
    @Body() { fileId, clientId, fileVersion }: FileSyncInitDto,
  ) {
    await this.fileSyncService.initializeFileSynchronizer(
      clientId,
      fileId,
      fileVersion,
    );
  }
}
