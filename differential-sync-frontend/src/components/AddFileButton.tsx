import {Button} from "@mui/material";
import {Add} from "@mui/icons-material";

export type AddFileButtonProps = {
  onClick: () => void;
}

function AddFileButton({ onClick }: AddFileButtonProps) {
  return (
    <Button
      onClick={onClick}
      sx={{
        position: "absolute",
        top: '10px',
        right: '30px',
      }}
      startIcon={<Add />}
    >
      Add file
    </Button>
  );
}

export default AddFileButton;