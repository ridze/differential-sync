import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileModule } from './file/file.module';
import { FileSyncModule } from './file-sync/file-sync.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { File } from './file/entities/file.entity';
import { Client } from './file-sync/entities/client.entity';
import { FileVersionSnapshot } from './file/entities/fileVersionSnapshot.entity';
import { ClientShadowFile } from './file-sync/entities/clientShadowFile.entity';
import { ClientBackupFile } from './file-sync/entities/clientBackupFile.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    FileModule,
    FileSyncModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [
        Client,
        File,
        FileVersionSnapshot,
        ClientShadowFile,
        ClientBackupFile,
      ],
      synchronize: true,
      // dropSchema: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
