import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { File } from './entities/file.entity';
import { FileVersionSnapshot } from './entities/fileVersionSnapshot.entity';

@Module({
  imports: [TypeOrmModule.forFeature([File, FileVersionSnapshot])],
  controllers: [FileController],
  providers: [FileService],
  exports: [FileService],
})
export class FileModule {}
