import {useCallback, useEffect, useState} from 'react'
import FileEditor from "./components/FileEditor.tsx";
import './App.css'
import axiosInstance from "./utils/axios.ts";
import FileCard from "./components/FileCard.tsx";
import {File} from "./types/file.ts";
import AddFileButton from "./components/AddFileButton.tsx";
import {WebSocketConnection} from "./utils/websocketConnection.ts";
import FileSynchronizer from "./utils/fileSynchronizer.ts";

function App() {
  const [file, setFile] = useState<File | null>(null);
  const [files, setFiles] = useState<File[]>([]);

  useEffect(() => {
    WebSocketConnection.ensureConnection(FileSynchronizer.onWebsocketMessage);
    return () => WebSocketConnection.disconnect();
  }, []);

  useEffect(() => {
    const getFiles = async () => {
      try {
        const filesResponse = await axiosInstance.get<File[]>('files');
        setFiles(filesResponse.data);
      } catch (e) {
        console.log('ERROR GETTING ALL FILES', e);
      }
    }

    getFiles();
  }, []);

  const onAddFile = useCallback(() => {
    (async () => {
      try {
        const addFileResponse = await axiosInstance.post<File>('files');
        setFiles((files) => [...files, addFileResponse.data]);
      } catch (e) {
        console.log('ERROR ADDING NEW FILES', e);
      }
    }) ()
  }, []);

  return (
      <div id="app">
        <div id="files-array-wrapper">
          {files.map((file) => (<FileCard key={file.id} file={file} onClick={() => setFile(file)} />))}
        </div>
        <div id="main-content-wrapper">
          {file && <FileEditor key={file.id} file={file} />}
        </div>
        <AddFileButton onClick={onAddFile} />
      </div>
  );
}

export default App
