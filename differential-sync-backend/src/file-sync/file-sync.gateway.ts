import { WebSocketGateway, SubscribeMessage } from '@nestjs/websockets';
import { FileSyncService } from './file-sync.service';
import { Socket } from 'socket.io';
import { ClientMessage } from './dto/file-sync-websocket-message.dto';

@WebSocketGateway(4000, {
  cors: 'http://localhost:5173',
  namespace: 'file-sync',
})
export class FileSyncGateway {
  constructor(private readonly fileSyncService: FileSyncService) {}

  handleConnection(socket: Socket) {
    console.log('CLIENT CONNECTED', socket.id);
    socket.send({
      payload: 'A test message from the server!',
    });

    this.fileSyncService.onClientConnected(socket.id, socket);
  }

  handleDisconnect(socket: Socket) {
    console.log('CLIENT DISCONNECTED', socket.id);
    this.fileSyncService.onClientDisconnected(socket.id);
  }

  @SubscribeMessage('message')
  onEvent(socket: Socket, message: ClientMessage) {
    console.log('MESSAGE ARRIVED', socket.id, message);
    this.fileSyncService.onMessage(socket.id, message);
  }
}
