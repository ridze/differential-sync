import {TextareaAutosize} from "@mui/base";
import {File} from "../types/file.ts";
import {ChangeEvent, useCallback, useEffect, useState} from "react";
import FileSynchronizer from "../utils/fileSynchronizer.ts";

export type FileDiff = [number, string];
export type FileDiffs = FileDiff[];

export type Edit = {
  diffs: FileDiffs;
  nCount: number;
  mCount: number;
};

// export class UpdateFileDto {
//   clientId: string;
//   fileId: string;
//   nCount: number;
//   mCount: number;
//   diffs: FileDiffs;
// }

export type FileEditorProps = {
  file: File;
};

export type Shadow = {
  text: string;
  nCount: number;
  mCount: number;
};

export type Backup = {
  text: string;
  nCount: number;
};

function FileEditor({ file }: FileEditorProps) {
  const [text, setText] = useState<string>(file.text);
  const fileSynchronizer = FileSynchronizer.getInstance(file);
  const [synchronizationInitialized, setSynchronizationInitialized] = useState(fileSynchronizer.synchronizationInitialized);

  const onSynchronized = useCallback(() => setSynchronizationInitialized(true), []);

  const onServerEditsApplied = useCallback((newText: string) => setText(newText), []);

  useEffect(() => {
    fileSynchronizer.initiateFileSync(onSynchronized, onServerEditsApplied);
    // return () => fileSynchronizer.removeSyncronizedCallback(onSynchronized);
  }, [fileSynchronizer, onSynchronized, onServerEditsApplied]);

  // Periodically sending client ACK messages (mCount), even if they are send when the client applies update, to cover
  // the packet lost scenario.
  useEffect(() => {
    const interval = setInterval(() => fileSynchronizer.sendClientAckSignal(), 10000);
    return () => clearInterval(interval);
  }, [fileSynchronizer]);

  // Calculate diffs and push to edits stack; update shadow and nCount;
  useEffect(() => {
    const interval= setInterval(() => fileSynchronizer.yieldAndSendEdits(), 2000);
    return () => clearInterval(interval);
  }, [fileSynchronizer]);

  const onTextChange = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
    setText(event.target.value);
    fileSynchronizer.onLocalTextChange(event.target.value);
  }, [fileSynchronizer]);

  return (
    <TextareaAutosize
      minRows={40}
      value={text}
      onChange={onTextChange}
    />
  );
}

export default FileEditor;