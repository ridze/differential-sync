import { Socket } from 'socket.io';
import { Backup, Edit, Shadow } from './dto/update-file.dto';
import { ServerMessageType } from './dto/file-sync-websocket-message.dto';
import DMP from './diffMatchPatch';
import MainText, { MainFile } from './mainText';

enum ApplyEditResult {
  SUCCESSFULLY_APPLIED = 'SUCCESSFULLY_APPLIED',
  ALREADY_APPLIED = 'ALREADY_APPLIED',
  FAILED_TO_APPLY = 'FAILED_TO_APPLY',
}

class FileSynchronizer {
  public fileId: string;
  private version: number;
  private shadow: Shadow;
  private backup: Backup;
  private edits: Edit[] = [];

  constructor(file: MainFile) {
    this.fileId = file.id;
    this.version = file.version;
    this.shadow = {
      text: file.text,
      nCount: 0,
      mCount: 0,
    };
    this.backup = {
      text: file.text,
      mCount: 0,
    };
  }

  private applyEdit(edit: Edit) {
    // If I already have that edit, ignore.
    if (edit.nCount < this.shadow.nCount) {
      return ApplyEditResult.ALREADY_APPLIED;
    }

    console.log('EDIT', edit, 'THIS N COUNT', this.shadow.nCount);

    if (
      edit.nCount === this.shadow.nCount &&
      edit.mCount === this.shadow.mCount
    ) {
      const patches = DMP.patch_make(this.shadow.text, edit.diffs);
      const [patchedText, patchesResults] = DMP.patch_apply(
        patches,
        this.shadow.text,
      );

      const appliedPatches = patches.filter(
        (patch, index) => patchesResults[index],
      );

      MainText.applyPatches(this.fileId, appliedPatches);

      this.shadow.text = patchedText;

      return ApplyEditResult.SUCCESSFULLY_APPLIED;
    } else {
      console.log('EDIT', edit, 'CANNOT BE APPLIED TO SHADOW', this.shadow);
      return ApplyEditResult.FAILED_TO_APPLY;
    }
  }

  // Client self edit
  private applyClientEdit(edit: Edit) {
    const result = this.applyEdit(edit);

    if (result === ApplyEditResult.SUCCESSFULLY_APPLIED) {
      this.shadow.nCount += 1;
    }

    return result;
  }

  // Other client edited
  private applyServerEdit(edit: Edit) {
    const result = this.applyEdit(edit);

    if (result === ApplyEditResult.SUCCESSFULLY_APPLIED) {
      this.shadow.mCount += 1;
    }

    return result;
  }

  // Apply all edits or apply them until one of them cannot be applied;
  private applyEdits(edits: Edit[]) {
    const results: ApplyEditResult[] = [];

    for (let i = 0; i < edits.length; i += 1) {
      const edit = edits[i];

      const result = this.applyClientEdit(edit);

      results.push(result);

      // Force exit only when failed to apply
      if (result === ApplyEditResult.FAILED_TO_APPLY) {
        return results;
      }
    }

    return results;
  }

  onLocalTextChange() {}

  // Apply edits to client shadow, if successful, to mainText and send ack to client
  onClientTextChange(edits: Edit[], socket: Socket) {
    this.applyEdits(edits);
    this.sendServerAckSignal(socket);
  }

  // Diff against the main text and save edits.
  // after each edit yielded, increment mCount and make shadowText = mainText.
  yieldEdits() {
    const mainText = MainText.getMainFile(this.fileId).text;

    console.log('DIFFING', this.shadow.text, mainText);
    const diffs = DMP.diff_main(this.shadow.text, mainText);

    if (diffs.length > 1) {
      console.log('GOT DIFFS', this.shadow.text, mainText, diffs);

      this.edits.push({
        diffs,
        mCount: this.shadow.mCount,
        nCount: this.shadow.nCount,
      });

      this.shadow.text = mainText;
      this.shadow.mCount += 1;
    }
  }

  sendEdits(socket: Socket) {
    if (this.edits.length > 0) {
      socket.send({
        fileId: this.fileId,
        type: ServerMessageType.SERVER_TEXT_CHANGE,
        payload: { edits: this.edits },
      });
    }
  }

  yieldAndSendEdits(socket: Socket) {
    this.yieldEdits();
    this.sendEdits(socket);
  }

  onClientAckSignal(mCount: number) {
    const lastAcknowledgedEditIndex = this.edits.findIndex(
      (e) => e.mCount === mCount,
    );

    if (lastAcknowledgedEditIndex >= 0) {
      this.edits = this.edits.slice(0, lastAcknowledgedEditIndex + 1);

      console.log(
        'CLEARING ACKNOWLEDGED EDITS FROM INDEX:',
        lastAcknowledgedEditIndex,
        'NEW EDITS:',
        this.edits,
      );
    }
  }

  sendServerAckSignal(socket: Socket): void {
    const message = {
      fileId: this.fileId,
      type: ServerMessageType.SERVER_ACK_SIGNAL,
      payload: { nCount: this.shadow.nCount },
    };

    console.log('SENDING MESSAGE', message);

    socket.send(message);
  }
}

export default FileSynchronizer;
