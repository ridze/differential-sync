import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { Edit } from './dto/update-file.dto';
import { FileService } from '../file/file.service';
import { ClientShadowFile } from './entities/clientShadowFile.entity';
import { ClientBackupFile } from './entities/clientBackupFile.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Client as ClientEntity } from './entities/client.entity';
import { Socket } from 'socket.io';
import {
  ClientMessage,
  ClientMessageType,
  ServerMessage,
} from './dto/file-sync-websocket-message.dto';
import FileSynchronizer from './fileSynchronizer';
import MainText from './mainText';

type ClientId = string;
type FileId = string;
type Client = {
  socket: Socket;
  fileSynchronizersMap: Record<FileId, FileSynchronizer>;
};

@Injectable()
export class FileSyncService {
  private readonly ackMessagesInterval: ReturnType<typeof setInterval>;
  private readonly clientsMap: Record<ClientId, Client> = {};

  @Inject(FileService)
  private readonly fileService: FileService;

  constructor(
    @InjectRepository(ClientEntity)
    private clientRepository: Repository<ClientEntity>,
    @InjectRepository(ClientShadowFile)
    private clientShadowFileRepository: Repository<ClientShadowFile>,
    @InjectRepository(ClientBackupFile)
    private clientBackupFileRepository: Repository<ClientBackupFile>,
  ) {
    this.ackMessagesInterval = setInterval(this.broadcastAckSignals, 10000);
    // TODO shadows/backups cleanup interval (loop through socketsMap values and remove all shadow & backups with id not included)
  }

  private getClient(clientId: string): Client | undefined {
    return this.clientsMap[clientId];
  }

  private getClientSocket(clientId: ClientId): Socket | undefined {
    return this.getClient(clientId)?.socket;
  }

  private getClientFileSynchronizer(
    clientId: ClientId,
    fileId: FileId,
  ): FileSynchronizer | undefined {
    return this.getClient(clientId)?.fileSynchronizersMap[fileId];
  }

  private setClient(clientId: ClientId, socket: Socket) {
    if (this.getClient(clientId)) {
      console.log('** CLIENT SET TWICE **', clientId);
    }

    this.clientsMap[clientId] = {
      socket,
      fileSynchronizersMap: {},
    };
  }

  private removeClient(clientId: ClientId) {
    if (this.getClientSocket(clientId)) {
      this.clientsMap[clientId].socket.disconnect();
      delete this.clientsMap[clientId];
    }
  }

  private iterateFileSynchronizers(
    callback: (fileSynchronizer: FileSynchronizer, socket: Socket) => void,
  ) {
    return Object.keys(this.clientsMap).forEach((clientId) => {
      const { socket, fileSynchronizersMap } = this.clientsMap[clientId];
      Object.keys(fileSynchronizersMap).forEach((fileId) => {
        const fileSynchronizer = fileSynchronizersMap[fileId];
        callback(fileSynchronizer, socket);
      });
    });
  }

  public onClientConnected(clientId: ClientId, socket: Socket) {
    this.setClient(clientId, socket);
  }

  public onClientDisconnected(clientId: ClientId) {
    this.removeClient(clientId);
  }

  private async initializeClientFileSynchronizer(
    clientId: ClientId,
    fileId: FileId,
    fileVersion: number,
  ) {
    // TODO get the right version
    this.clientsMap[clientId].fileSynchronizersMap[fileId] =
      new FileSynchronizer(MainText.getMainFile(fileId));
  }

  public onMessage(clientId: ClientId, message: ClientMessage) {
    if (this.getClientSocket(clientId)) {
      switch (message.type) {
        case ClientMessageType.CLIENT_TEXT_CHANGE: {
          this.onClientTextChange(
            clientId,
            message.fileId,
            message.payload.edits,
          );
          break;
        }
        case ClientMessageType.CLIENT_ACK_SIGNAL: {
          this.onClientAckSignal(
            clientId,
            message.fileId,
            message.payload.mCount,
          );
          break;
        }
      }
    }
  }

  public sendMessage(clientId: ClientId, message: ServerMessage) {
    this.getClientSocket(clientId)?.send(message);
  }

  async initializeFileSynchronizer(
    clientId: ClientId,
    fileId: FileId,
    fileVersion: number,
  ): Promise<void> {
    if (this.getClientSocket(clientId)) {
      await this.initializeClientFileSynchronizer(
        clientId,
        fileId,
        fileVersion,
      );
    } else {
      throw new BadRequestException(
        `Socket connection with clientId ${clientId} is not established!`,
      );
    }
  }

  async yieldAndBroadcastFileEdits(fileId: FileId, excludeClientId?: ClientId) {
    this.iterateFileSynchronizers((fileSynchronizer, socket) => {
      if (
        fileSynchronizer.fileId === fileId &&
        (!excludeClientId || excludeClientId !== socket.id)
      ) {
        console.log('YIELD AND SEND EDITS', socket.id);
        fileSynchronizer.yieldAndSendEdits(socket);
      }
    });
  }

  // Synchronize the client that changed the text, then broadcast to all the other clients
  async onClientTextChange(clientId: ClientId, fileId: FileId, edits: Edit[]) {
    const clientFileSynchronizer = this.getClientFileSynchronizer(
      clientId,
      fileId,
    );

    if (clientFileSynchronizer) {
      clientFileSynchronizer.onClientTextChange(
        edits,
        this.getClientSocket(clientId),
      );
      this.yieldAndBroadcastFileEdits(fileId, clientId);
    }
  }

  async onClientAckSignal(clientId: ClientId, fileId: FileId, mCount: number) {
    this.getClientFileSynchronizer(clientId, fileId)?.onClientAckSignal(mCount);
  }

  // Periodically send unacknowledged edits to all the clients, in case edits sent when they got performed got lost.
  broadcastEdits = async () => {
    this.iterateFileSynchronizers((fileSynchronizer, socket) =>
      fileSynchronizer.sendEdits(socket),
    );
  };

  // Periodically send ack signals to all the clients, in case ack signal sent on edit got lost.
  broadcastAckSignals = async () => {
    this.iterateFileSynchronizers((fileSynchronizer, socket) =>
      fileSynchronizer.sendServerAckSignal(socket),
    );
  };
}
