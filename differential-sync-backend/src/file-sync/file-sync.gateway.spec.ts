import { Test, TestingModule } from '@nestjs/testing';
import { FileSyncGateway } from './file-sync.gateway';
import { FileSyncService } from './file-sync.service';

describe('FileSyncGateway', () => {
  let gateway: FileSyncGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileSyncGateway, FileSyncService],
    }).compile();

    gateway = module.get<FileSyncGateway>(FileSyncGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
