import {diff_match_patch} from "diff-match-patch";
import {File, FileSyncInitDto} from "../types/file.ts";
import {Backup, Edit, Shadow} from "../components/FileEditor.tsx";
import {ServerMessage, ServerMessageType, ClientMessageType, WebSocketConnection} from "./websocketConnection.ts";
import axiosInstance from "./axios.ts";
import {noop} from "lodash";

const DMP = new diff_match_patch();

enum ApplyEditResult {
  SUCCESSFULLY_APPLIED = 'SUCCESSFULLY_APPLIED',
  ALREADY_APPLIED = 'ALREADY_APPLIED',
  FAILED_TO_APPLY = 'FAILED_TO_APPLY',
}

class FileSynchronizer {
  public fileId: string;
  private text: string;
  private version: number;
  private shadow: Shadow;
  private backup: Backup;
  private edits: Edit[] = [];
  private static synchronizersMap: Record<string, FileSynchronizer> = {};
  private onSynchronizedCallbacks: (() => void)[] = [];
  public synchronizationInitialized = false;
  public onServerEditsApplied: (newText: string) => void = noop;

  constructor(file: File) {
    this.fileId = file.id;
    this.text = file.text;
    this.version = file.version;
    this.shadow = {
      text: file.text,
      nCount: 0,
      mCount: 0
    };
    this.backup = {
      text: file.text,
      nCount: 0
    };
  }

  public static getInstance(file: File) {
    if (!this.synchronizersMap[file.id]) {
      this.synchronizersMap[file.id] = new FileSynchronizer(file);
    }

    return this.synchronizersMap[file.id];
  }

  public static getExistingInstance(fileId: string): FileSynchronizer | undefined {
    return this.synchronizersMap[fileId];
  }

  public static onWebsocketMessage(message: ServerMessage) {
    const fileSynchronizer = FileSynchronizer.getExistingInstance(message.fileId);

    // If not for current file, ignore
    if (!fileSynchronizer || message.fileId !== fileSynchronizer.fileId) {
      return;
    }

    switch (message.type) {
      case ServerMessageType.SERVER_TEXT_CHANGE: {
        console.log(message);
        fileSynchronizer.onServerTextChange(message.payload.edits);
        break;
      }
      case ServerMessageType.SERVER_ACK_SIGNAL: {
        console.log(message);
        fileSynchronizer.onServerAckSignal(message.payload.nCount);
        break;
      }
    }
  }

  async initiateFileSync(onSynchronized: () => void, onServerEditsApplied: (newText: string) => void ) {
    this.onSynchronizedCallbacks.push(onSynchronized);
    this.onServerEditsApplied = onServerEditsApplied;

    try {
      // TODO return from BE and set the fresh text here
      await axiosInstance.post<FileSyncInitDto>('file-sync-init', {
        clientId: WebSocketConnection.clientId,
        fileId: this.fileId,
        fileVersion: this.version,
      });
    } catch (e) {
      console.log('INITIATE FILE SYNC ERROR', e);
    }
  }

  onLocalTextChange(newText: string) {
    this.text = newText;
  }

  private applyEdit(edit: Edit) {
    console.log('TRY TO APPLY EDIT: ', edit, 'TO SHADOW:', { ... this.shadow });

    // If I already have that edit, ignore.
    if (edit.mCount < this.shadow.mCount) {
      return ApplyEditResult.ALREADY_APPLIED;
    }

    if (
      edit.nCount === this.shadow.nCount &&
      edit.mCount === this.shadow.mCount
    ) {
      // PATCH SHADOW
      const shadowPatches = DMP.patch_make(this.shadow.text, edit.diffs);
      const [patchedShadowText, shadowPatchesResults] = DMP.patch_apply(
        shadowPatches,
        this.shadow.text,
      );

      this.shadow.text = patchedShadowText;

      // PATCH MAIN TEXT
      const textPatches = DMP.patch_make(this.text, edit.diffs);
      const [patchedText, patchesResults] = DMP.patch_apply(
        textPatches,
        this.text,
      );

      this.text = patchedText;

      return ApplyEditResult.SUCCESSFULLY_APPLIED;
    } else {
      console.log('EDIT', edit, 'CANNOT BE APPLIED TO SHADOW', { ...this.shadow });
      return ApplyEditResult.FAILED_TO_APPLY;
    }
  }

  // Apply all edits or apply them until one of them cannot be applied;
  private applyServerEdits(edits: Edit[]) {
    const results: ApplyEditResult[] = [];

    for (let i = 0; i < edits.length; i += 1) {
      const edit = edits[i];

      const result = this.applyEdit(edit);

      results.push(result);

      if (result === ApplyEditResult.SUCCESSFULLY_APPLIED) {
        this.shadow.mCount += 1;
      } else if (result === ApplyEditResult.FAILED_TO_APPLY) {
        return results;
      }
    }

    return results;
  }

  // Apply all the edits to shadow, if possible. send ack signal to server (mCount)
  onServerTextChange(edits: Edit[]) {
    const successfulEdits = this.applyServerEdits(edits);

    // If there were any successfully applied edits patch call the hook
    if (successfulEdits.find(e => e === ApplyEditResult.SUCCESSFULLY_APPLIED)) {
      this.onServerEditsApplied(this.text);
    }

    this.sendClientAckSignal();
  }

  yieldEdit() {
    console.log('YIELD EDIT');

    // console.log('DIFFING', this.shadow.text, this.text)
    const diffs = DMP.diff_main(this.shadow.text, this.text);

    if (diffs.length > 1) {
      const newEdit = {
        diffs,
        nCount: this.shadow.nCount,
        mCount: this.shadow.mCount
      };

      console.log('YIELDED', newEdit);

      this.edits.push(newEdit);
      this.shadow.text = this.text;
      this.shadow.nCount += 1;
    }
  }

  sendEdits() {
    if (this.edits.length > 0) {
      WebSocketConnection.sendMessage({
        fileId: this.fileId,
        type: ClientMessageType.CLIENT_TEXT_CHANGE,
        payload: { edits: this.edits }
      });
    }
  }

  yieldAndSendEdits() {
    this.yieldEdit();
    this.sendEdits();
  }

  onServerAckSignal(nCount: number) {
    // Remove acknowledged edits. No need to send them anymore.
    const newEdits = this.edits.filter(edit => edit.nCount > nCount);
    if (newEdits.length !== this.edits.length) {
      console.log('REMOVING ACKED EDITS. NEW EDITS:', newEdits);
    }
    this.edits = newEdits;
  }

  sendClientAckSignal(): void {
    WebSocketConnection.sendMessage({
      fileId: this.fileId,
      type: ClientMessageType.CLIENT_ACK_SIGNAL,
      payload: { mCount: this.shadow.mCount }
    });
  }
}

export default FileSynchronizer;