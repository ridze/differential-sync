export class FileSyncInitDto {
  clientId: string;
  fileId: string;
  fileVersion: number;
}
