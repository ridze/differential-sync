import { Module } from '@nestjs/common';
import { FileSyncService } from './file-sync.service';
import { FileSyncGateway } from './file-sync.gateway';
import { FileModule } from '../file/file.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Client } from './entities/client.entity';
import { ClientShadowFile } from './entities/clientShadowFile.entity';
import { ClientBackupFile } from './entities/clientBackupFile.entity';
import { FileSyncController } from './file-sync.controller';

@Module({
  imports: [
    FileModule,
    TypeOrmModule.forFeature([Client, ClientShadowFile, ClientBackupFile]),
  ],
  controllers: [FileSyncController],
  providers: [FileSyncGateway, FileSyncService],
})
export class FileSyncModule {}
