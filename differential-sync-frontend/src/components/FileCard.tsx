import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import { red } from '@mui/material/colors';
import {File} from "../types/file.ts";

export type FileCardProps = {
  file: File;
  onClick: () => void;
};

export default function FileCard({ file, onClick }: FileCardProps) {
  return (
    <Card sx={{ minWidth: '220px', width: '220px', marginRight: '10px', cursor: 'pointer' }} onClick={onClick}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            {file.name.charAt(0).toUpperCase()}
          </Avatar>
        }
        title={file.name}
        subheader="September 14, 2016"
      />
    </Card>
  );
}