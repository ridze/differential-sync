import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { FileService } from './file.service';
import { CreateFileDto } from './dto/create-file.dto';
import MainText from '../file-sync/mainText';

@Controller('files')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post()
  async create(@Body() createFileDto: CreateFileDto) {
    try {
      const file = await this.fileService.create(createFileDto);

      MainText.setMainFile(file.id, {
        id: file.id,
        name: file.name,
        text: file.text,
        version: file.version,
      });

      return MainText.getMainFile(file.id);
    } catch (e) {
      throw e;
    }
  }

  @Get()
  async findAll() {
    if (!MainText.filesCached) {
      const files = await this.fileService.findAll();
      files.forEach((file) =>
        MainText.setMainFile(file.id, {
          id: file.id,
          name: file.name,
          text: file.text,
          version: file.version,
        }),
      );
    }

    return MainText.getFilesArray();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileService.findOne(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fileService.remove(id);
  }
}
