import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ClientShadowFile } from './clientShadowFile.entity';
import { ClientBackupFile } from './clientBackupFile.entity';

@Entity('clients')
export class Client {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  name: string;

  @OneToMany(
    () => ClientShadowFile,
    (clientShadowFile) => clientShadowFile.client,
  )
  shadowFiles: ClientShadowFile[];

  @OneToMany(
    () => ClientBackupFile,
    (clientBackupFile) => clientBackupFile.file,
  )
  backupFiles: ClientBackupFile[];
}
